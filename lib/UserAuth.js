const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');

const schema = new Schema({
  username: {
    type: String,
    required: true,
    index: {
      unique: true,
      sparse: true
    }
  },
  password: {
    type: String,
    required: true
  }
}, {
  toJSON: {
    transform: function (doc, ret) {
      delete ret.password;
      return ret;
    },
    versionKey: false
  }
});

schema.pre('save', async function (next) {
  if (this.isNew) {
    try {
      await this.hashPassword();
    } catch (err) {
      return next(err);
    }
    next();
  }
});

schema.methods.validatePassword = function (password) {
  return bcrypt.compare(password, this.password);
};

schema.methods.hashPassword = async function () {
  if (!this.password) {
    return;
  }
  const salt = await bcrypt.genSalt(10);
  this.password = await bcrypt.hash(this.password, salt);
};

module.exports = mongoose.model('UserAuth', schema);
